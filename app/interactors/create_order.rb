class CreateOrder
  include Interactor

  def call
    order = context.order.to_kashflow
    order[:customer_id] = context.customer_id
    context.client.insert_invoice(order)
  end
end