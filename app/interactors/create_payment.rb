class CreatePayment
  include Interactor

  def call
    context.order.payments.each do |payment|
      context.client.insert_invoice_payment(payment.to_kashflow)
    end

    context.order.touch :kashflow_synced_at
  end
end