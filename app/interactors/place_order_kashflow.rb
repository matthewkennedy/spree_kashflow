class PlaceOrderKashflow
  include Interactor::Organizer

  organize Authenticate, CreateCustomer, CreateOrder, CreateItems, CreatePayment
end
