class CreateItems
  include Interactor

  def call
    context.order.line_items.each do |item|
      context.client.insert_invoice_line_with_invoice_number(item.to_kashflow)
    end

    context.order.line_item_adjustments.each do |adjustment|
      context.client.insert_invoice_line_with_invoice_number(adjustment.to_kashflow)
    end

    context.order.adjustments.each do |adjustment|
      context.client.insert_invoice_line_with_invoice_number(adjustment.to_kashflow)
    end

    context.client.insert_invoice_line_with_invoice_number(context.order.shipment_to_kashflow)
  end
end