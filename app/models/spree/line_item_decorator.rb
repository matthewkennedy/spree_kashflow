module Spree
  LineItem.class_eval do
    def to_kashflow
      {
        invoice_number: order.kashflow_number,
        quantity: quantity,
        description: [variant.name, variant.options_text].join(" "),
        rate: price.to_f,
        values_in_currency: order.gbp? ? 0 : 1,
        charge_type: Spree::Config[:kashflow_sale_of_goods_code]
      }
    end
  end
end