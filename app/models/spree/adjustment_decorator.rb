module Spree
  Adjustment.class_eval do
    def to_kashflow
      {
        invoice_number: order.kashflow_number,
        quantity: 1,
        description: label,
        rate: amount.to_f,
        values_in_currency: order.gbp? ? 0 : 1,
      }
    end
  end
end